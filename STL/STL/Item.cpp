#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice) :
	_name(name),
	_serialNumber(serialNumber),
	_unitPrice(unitPrice),
	_count(1)
{}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) < 0); // if returned value is smaller than 0, the first string is lower
}

bool Item::operator>(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) > 0); // if returned value is bigger than 0, the first string is greater
}

bool Item::operator==(const Item& other) const
{
	return (this->_serialNumber.compare(other._serialNumber) == 0); // if returned value is equal to 0, the strings are the same
}

std::string Item::getName() const
{
	return this->_name;
}

std::string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getAmount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

void Item::setName(const std::string newName)
{
	this->_name = newName;
}

void Item::setSerialNumber(const std::string newSerialNumber)
{
	this->_serialNumber = newSerialNumber;
}

void Item::setUnitPrice(const double newUnitPrice)
{
	this->_unitPrice = newUnitPrice;
}

void Item::addAnotherUnit()
{
	this->_count++;
}

void Item::removeAUnit()
{
	this->_count--;
}

std::ostream& operator<<(std::ostream& out, const Item& item)
{
	out << "name: " << item.getName();
	for (int i = item.getName().length(); i < 9; i++)
	{
		out << " ";
	}

	out << "  |  price: " << item.getUnitPrice();
	for (int i = 0; i < 6; i++)
	{
		out << " ";
	}

	out << "  |  amount: " << item.getAmount();
	for (int i = 0; i < 6; i++)
	{
		out << " ";
	}

	out << "  |  total price: " << item.totalPrice() << "  |\n";
	
	
	return out;
}
