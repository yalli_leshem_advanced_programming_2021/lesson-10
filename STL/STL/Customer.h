#pragma once

#include"Item.h"
#include<set>

class Customer
{
public:
	Customer() = default;
	Customer(std::string name);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item newItem);//add item to the set
	void removeItem(Item item);//remove item from the set

	//get and set functions
	std::string getName() const;
	std::set<Item> getItems() const;

private:
	std::string _name;
	std::set<Item> _items;
};