#include "Customer.h"

Customer::Customer(std::string name) : _name(name)
{}

Customer::~Customer()
{
	this->_items.clear();
}

double Customer::totalSum() const
{
	double sum = 0;

	for (Item item : this->_items)
	{
		sum += item.totalPrice();
	}

	return sum;
}

void Customer::addItem(Item newItem)
{
	for (Item item : this->_items) // got through all the items in the list t4o check if this item is a duplicate 
	{
		if (item == newItem) // if item is already in the list 
		{
			item.addAnotherUnit(); // update the count
			this->_items.erase(item); // remove the outdated item
			this->_items.insert(item); // update the item in the list

			return;
		}
	}

	this->_items.insert(newItem); // if didn't return already the item isn't in the list, add it to it 
}

void Customer::removeItem(Item item)
{
	for (Item it : this->_items)
	{
		if (item == it) // when item is found
		{
			this->_items.erase(it);

			it.removeAUnit(); // remove one unit
			if (it.getAmount() > 0) // if theres more units
			{
				this->_items.insert(it); // add the item back with the updated amount of units
			}
			break;
		}
	}
}

std::string Customer::getName() const
{
	return this->_name;
}

std::set<Item> Customer::getItems() const
{
	return this->_items;
}
