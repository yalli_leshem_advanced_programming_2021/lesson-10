#include"Customer.h"
#include<map>

int printMenu();

void printItemsList(const std::set<Item> itemList);

void addNewCustomer(std::map<std::string, Customer>& abcCustomer, const std::set<Item> itemList); // choice 1
void updateCustomer(std::map<std::string, Customer>& abcCustomer, const std::set<Item> itemList); // choice 2
void printHighestPayingCustomer(const std::map<std::string, Customer> abcCustomer); // choice 3

void removeItems(Customer& customer);
void addItems(Customer& customer, const std::set<Item> itemList); // used both by choices 1 and 2

int main()
{
	const int SIGN_IN = 1;
	const int UPDATE_CUSTOMER = 2;
	const int PRINT_HIGHEST_PAYING_COSTUMER = 3;
	const int EXIT_CODE = 4;
	int choice = 0;

	std::map<std::string, Customer> abcCustomers;

	std::set<Item> itemList;
	itemList.insert( Item("Milk", "00001", 5.3) );
	itemList.insert( Item("Cookies", "00002", 12.6) );
	itemList.insert( Item("bread", "00003", 8.9) );
	itemList.insert( Item("chocolate", "00004", 7.0) );
	itemList.insert( Item("cheese", "00005", 15.3) );
	itemList.insert( Item("rice", "00006", 6.2) );
	itemList.insert( Item("chicken", "00007", 25.99) );
	itemList.insert( Item("fish", "00008", 31.65) );
	itemList.insert( Item("cucumber", "00009", 1.21) );
	itemList.insert( Item("tomato", "00010", 2.32) );

	while (EXIT_CODE != choice) // stop if user wants to exit
	{
		choice = printMenu(); // gets a valid choice from the user

		switch (choice)
		{
		case SIGN_IN: // choice 1
			addNewCustomer(abcCustomers, itemList); // add a new customer 
			break;

		case UPDATE_CUSTOMER: // choice 2
			updateCustomer(abcCustomers, itemList); // update the information of an existing customer
			break;

		case PRINT_HIGHEST_PAYING_COSTUMER: // choice 3
			printHighestPayingCustomer(abcCustomers); // print the information about the customer that pays the most
			break;

		default: // the user can't input an invalid number, so default is just break
			break;
		}
	}

	return 0;
}

/// <summary>
/// print the menu to the user 
/// makes sure the choice us valid (1-4)
/// </summary>
/// <returns>a valid choice the user makes (1-4)</returns>
int printMenu()
{
	const int MIN_CHOICE = 1;
	const int MAX_CHOICE = 4;
	int choice = 0;

	// print options
	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1. to sign as customer and buy items" << std::endl;
	std::cout << "2. to update existing customer's items" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit" << std::endl;

	while (!(MIN_CHOICE <= choice && choice <= MAX_CHOICE)) // do until choice is valid
	{
		std::cout << "Enter a valid option (1-4): ";
		std::cin >> choice;
		getchar(); // clean buffer
	}

	return choice;
}

/// <summary>
/// gets a list of items and prints them out
/// </summary>
void printItemsList(const std::set<Item> itemList)
{
	int counter = 1;

	for (Item item : itemList) // go through the items in the list
	{
		std::cout << counter << ") " << item << std::endl; // print the item

		counter++;
	}
}

/// <summary>
/// adds a new customer
/// </summary>
/// <param name="abcCustomer">the container of all the customers</param>
/// <param name="itemList">the list of all the items</param>
void addNewCustomer(std::map<std::string, Customer>& abcCustomer, const std::set<Item> itemList)
{
	// get new customer name
	std::string name;
	std::cout << "Enter customer name: ";
	std::getline(std::cin, name);

	if (abcCustomer.count(name) == 1) // if customer already exists
	{
		std::cout << "Error: Customer already exists!" << std::endl; // print error
		return; // go back to menu
	}

	abcCustomer.insert(std::pair<std::string, Customer>(name, Customer(name)));
	addItems(abcCustomer[name], itemList); // add items
}

/// <summary>
/// add/remove items from an existing customer
/// </summary>
/// <param name="abcCustomer">the container of all the customers</param>
/// <param name="itemList">the list of items</param>
void updateCustomer(std::map<std::string, Customer>& abcCustomer, const std::set<Item> itemList)
{
	const int ADD_ITEMS = 1;
	const int REMOVE_ITEMS = 2;
	const int BACK_TO_MENU = 3;

	int option = 0;
	std::string name;

	// get new customer name
	std::cout << "Enter customer name: ";
	std::getline(std::cin, name);

	if (abcCustomer.count(name) == 0) // if customer isn't in the container
	{
		std::cout << "Error: Customer not found!" << std::endl; // print error
		return; // go back to menu
	}

	while (option != BACK_TO_MENU)
	{
		printItemsList(abcCustomer[name].getItems());

		std::cout << "\n1. Add items" << std::endl;
		std::cout << "2. Remove items" << std::endl;
		std::cout << "3. Back to menu" << std::endl;
		
		while (!(ADD_ITEMS <= option && option <= BACK_TO_MENU)) // do until choice is valid
		{
			std::cout << "Enter a valid option (1-3): ";
			std::cin >> option;
			getchar(); // clean buffer
		}

		switch (option)
		{
		case ADD_ITEMS: // choice 1
			addItems(abcCustomer[name], itemList); // add more items to the customer
			break;

		case REMOVE_ITEMS: // choice 2
			removeItems(abcCustomer[name]); // remove items from the customer
			break;

		default:
			break;
		}
	}
}

/// <summary>
/// remove items
/// </summary>
/// <param name="customer">the customer we want to remove items from</param>
void removeItems(Customer& customer)
{
	int itemIndex = -1;

	while (itemIndex != 0)
	{
		// print the items we got
		std::cout << "Your items: " << std::endl;
		printItemsList(customer.getItems()); 

		std::cout << "What item would you like to sell? (0 to exit) input: ";
		std::cin >> itemIndex;
		getchar(); // clear buffer

		// find the item in the list
		int counter = 1;
		for (Item item : customer.getItems())
		{
			if (counter == itemIndex) // if we found the item, remove it
			{
				customer.removeItem(item);
			}

			counter++;
		}
	}
}

/// <summary>
/// add items to the customers list
/// </summary>
/// <param name="customer">the customer</param>
/// <param name="itemList">the options</param>
void addItems(Customer& customer, const std::set<Item> itemList)
{
	int index = -1;

	// print options
	std::cout << "The items you can buy are: (0 to exit)" << std::endl;
	printItemsList(itemList);

	while (index != 0)
	{
		std::cout << "What item would you like to buy? Input:" << std::endl;
		std::cin >> index;
		getchar();

		// find the item in the list
		int counter = 1;
		for (Item item : itemList)
		{
			if (counter == index) // if we found the item, add it
			{
				customer.addItem(item);
			}

			counter++;
		}
	}
}

/// <summary>
/// prints the customer who paid the most
/// </summary>
/// <param name="abcCustomer"></param>
void printHighestPayingCustomer(const std::map<std::string, Customer> abcCustomer)
{
	Customer highestPaying = Customer("eeeg");

	for (std::pair<std::string, Customer> customerPair : abcCustomer)
	{
		Customer customer = customerPair.second; // second is the customer object
		highestPaying = (highestPaying.totalSum() > customer.totalSum()) ? highestPaying : customer; // get the one thats paying more
	}

	std::cout << "Highest paying customer is: " << highestPaying.getName() << std::endl;
	std::cout << "Sum total: " << highestPaying.totalSum() << std::endl;
	std::cout << "items: " << std::endl;
	printItemsList(highestPaying.getItems());
}