#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice);
	~Item() = default;

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	friend std::ostream& operator << (std::ostream& out, const Item& item); // printing operator

	//get and set functions
	std::string getName() const;
	std::string getSerialNumber() const;
	int getAmount() const;
	double getUnitPrice() const;

	void setName(const std::string newName);
	void setSerialNumber(const std::string newSerialNumber);
	void setUnitPrice(const double newUnitPrice);
	void addAnotherUnit();
	void removeAUnit();


private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};